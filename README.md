# automatic-email-reviews

This will send emails daily to guests who stayed 1 week ago, then mark them off so they don't receive more emails. Edit the email.html to edit the template.

The emails that have been sent are tracked with the email_cron table in the database

This plguin was designed and written for a weebsite using the wp_hostel plugin, however it shouldn't be too hard to adapt for different situations.

FEATURES:

On Plugin Activation:
creates DB for email tracking
creates a wp_option in DB for database version control

On Run:
Runs the scheduled daily task using the wordpress cron manager
SELECTS the guests that need to be emailed
Generates the emails with the HTML template, guest names and email addresses
Sends the emails in a loop with a delay of 3 seconds, and inserts records into the email tracking table "**_email_cron)"

On Plugin Deactivation:
removes the cron job
removes the database version from the wp_options table