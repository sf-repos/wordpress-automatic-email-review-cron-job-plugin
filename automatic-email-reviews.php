<?php
/**
* @package Automatic_Email_Reviews
*/
/*
Plugin Name: Automatic Email Reviews: WP Hostel Plugin Extension
Plugin URI: https://bitbucket.org/sf-repos/wordpress-automatic-email-review-cron-job-plugin/
Description: This sends out automatic emails prompting guests to leave reviews one week after their stay.
Version: 1.0
Author: Sam Fullalove
Author URI: http://sam.fullalove.co/
License: GPLv2 or later
Text Domain: automatic-email-reviews
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

/**
* Prevent exposer of any info if called directly
*/
if ( !function_exists( 'add_action' ) ) {
	echo 'I\'m just a plugin, don\'t call me directly.';
	exit;
}

define( 'AUTOMATIC_EMAIL_REVIEWS_VERSION', '1.0' );
define( 'AUTOMATIC_EMAIL_REVIEWS___MINIMUM_WP_VERSION', '3.2' );
define( 'AUTOMATIC_EMAIL_REVIEWS___PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'AUTOMATIC_EMAIL_REVIEWS_DELETE_LIMIT', 100000 );
global $aer_db_version;
$aer_db_version = '1.0';

require_once('class.mail-cron.php');

$mailCron = new mailCron;

/*plugin activation and deactivation hooks*/
register_activation_hook( __FILE__, array( $mailCron,'addTable_aer_Sent_Emails') );
register_deactivation_hook( __FILE__, array( $mailCron,'removeCron') );

/*schedule the cron daily*/
if ( ! wp_next_scheduled( 'reviewEmail' ) ) {
	wp_schedule_event( time(), 'daily', 'reviewEmail' );
}

add_action( 'reviewEmail', array( $mailCron, 'runCron') );


?>
