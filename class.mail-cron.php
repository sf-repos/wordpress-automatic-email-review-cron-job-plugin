<?php

class mailCron{

	/*******************************************************/
	/**
	*  Plugin activation code: Create the aer_sent_email table
	*/
	public function addTable_aer_Sent_Emails()
	{
		global $wpdb;
		global $aer_db_version;

		$charset_collate = $wpdb->get_charset_collate();
		$table_name = $wpdb->prefix .'aer_sent_emails';

		/* check if the table already exists */
		if($wpdb->get_var("show tables like '$table_name'") != $table_name)
		{
			$sql = "CREATE TABLE " .$table_name ." (
			id bigint(20) NOT NULL AUTO_INCREMENT,
			booking_id bigint(20) NOT NULL,
			email_sent tinyint(1) NOT NULL,
			UNIQUE KEY id (id)
			) $charset_collate;";

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );
		}

		add_option( 'aer_db_version', $aer_db_version );
	}

	/***************************************************************/
	/**
	*  Plugin deactivation code: delete the scheduled cron job and delete the db version option
	*/
	public function removeCron()
	{
		wp_clear_scheduled_hook('reviewEmail');

		delete_option( 'aer_db_version' );
	}
	/***************************************************************/
	/**
	* This function does 2 things:
	* 1. Gets a list of people who left 1 week ago and haven't been emailed yet
	* 2. Loops and emails these people while saving the progress in the aer_sent_emails table
	*/

	public function runCron()
	{
		global $wpdb;
		$date = date('Y-m-d',strtotime("-1 week"));

		/*This query gets the booking_id and contact_email of guests who left 1 week ago or more AND haven't been emailed yet*/
		$results = $wpdb->get_results( $wpdb->prepare( "
		SELECT ".$wpdb->prefix."wphostel_bookings.id, ".$wpdb->prefix."wphostel_bookings.contact_email, ".$wpdb->prefix."wphostel_bookings.contact_name
		FROM ".$wpdb->prefix."wphostel_bookings
		LEFT JOIN ".$wpdb->prefix."aer_sent_emails
		ON ".$wpdb->prefix."wphostel_bookings.id= ".$wpdb->prefix."aer_sent_emails.booking_id
		WHERE ".$wpdb->prefix."wphostel_bookings.to_date < %s
		&& (".$wpdb->prefix."aer_sent_emails.email_sent IS NULL
		OR ".$wpdb->prefix."aer_sent_emails.email_sent = 0) ", $date ) );

		/*uncomment this for debugging - prints results of above query*/
		foreach ( $results as $result ) { echo "<p>id: $result->id  email: $result->contact_email</p>";}

		/***************************************************************/

		/*build as much of the email as we can outside of the loop*/
		ob_start();
		include 'email.html';
		$bodyTemplate = ob_get_clean();
		$subject = 'Thank You For Staying At The Hostel';
		//$headers = array('Content-Type: text/html; charset=UTF-8');

		$headers[] = 'Content-Type: text/html; charset=UTF-8';
		$headers[] = 'From: Fred Bloggs <fred@blogshostel.com>';
		$headers[] = 'Reply-To: Fred Bloggs <fredbloggs@gmail.com>';

		/*loop and email the guests from the above query*/
		foreach ( $results as $result ) {
			if($result->contact_email != NULL)
			{
				/*set email variables*/
				$to = $result->contact_email;

				if($result->contact_name != NULL)
				{
					$fullName = $result->contact_name;
					$nameParts = explode(' ',trim($fullName));
					$firstName = $nameParts[0];
				}
				else {
					$firstName = ' ';
				}

				$body = str_replace("[[[NAME]]]", $firstName, $bodyTemplate); /*replace the name field with their name in the template*/
				wp_mail( $to, $subject, $body, $headers );

				/*uncomment this for debugging - prints who has been emailed*/
				echo "emailed $result->contact_email";

				/*insert cron status as it happens in case the scripts times out. This way even if it times out it can carry on next time.*/
				$wpdb->insert(
				$wpdb->prefix.'aer_sent_emails',
				array(
					'booking_id' => $result->id,
					'email_sent' => 1,
				),
				array(
					'%d',
					'%d'
				)
			);

			sleep(3); /*Sleep for 3 seconds in between emails so we are not marked as spam/ blacklisted...*/

		}
	}

}


}



?>
